# Mitarbeiterschulung-OMB

To participate in the hands-on session you do some very easy steps to set up the programming environment.  
You don't need to install any programm on your computer. 

There are three ways to go:

- You can login, with via RWTH-SSO on via this address: https://jupyter.rwth-aachen.de
- Alternatively, you can use the free available web-version of Jupyter here: https://jupyter.org/try.
- Use a locally installed instance of Jupyter Notebooks on you machine.

For the last case, an explanation how to do the setup is omitted.

## Using RWTH Jupyter

After that, you are asked to select a *profile*. Choose the already selected one called *Python*, scroll down to the very end and hit the *START* button.

After the initialization is finished (this might take a while), clone this Git-Repository by clicking the button marked in the image below and copy the url of this repository into the input field.

![Clone-Button](./img/jupiter-git.PNG)

After that, you should see the a folder "mitarbeiterschulung-omb" in your file browser at the left side. Go into this folder, where you should the files of this repository.

Finish the setup, by opening the notebook *01-Setup* and execute the Python-cell installing the required packages. For that, select the cell and hit the "run" button (c.f. image below).

![Run-Button](./img/Execute.png)

## Using JupyterLab

If you do this you should, do the setup immediately before the Hands-On session. If you close the windows, everything is lost.

Select *JupyterLab* to proceed.

While the lab is initializing, download the repository and extract it on your local machine. After initialization upload the files to *Lab*. You can simply drag and drop them to the file browser at the left. Unfortunately, you can't upload the *img* folder at once. So, create a folder called image and place the images there.

Finish the setup, by opening the notebook *01-Setup* and execute the Python cell installing the required packages. For that, select the sell and hit the "run" button (c.f. image below).

![Run-Button](./img/Execute.png)
